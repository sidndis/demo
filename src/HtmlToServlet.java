import java.io.*;
public class HtmlToServlet 
{
    public static void main(String[] args)
    {
        try
        {
            FileReader fr = new FileReader("F:\\abc.txt");
            BufferedReader br = new BufferedReader(fr);
            
            FileWriter fw = new FileWriter("F:\\new.txt");
            PrintWriter pw = new PrintWriter(fw);
            
            String s;
            String n;
            while(true)
            {
                s = br.readLine();
                if(s==null)
                    break;
                if(s.contains("\""))
                {
                    s = s.replace("\"", "\\\"");
                }
                n = "out.println(\""+s+"\");";
                pw.println(n);
            }
            pw.flush();
            
            pw.close();
            fw.close();
            br.close();
            fr.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
