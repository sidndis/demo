
import java.net.InetAddress;
import java.util.StringTokenizer;

class IPAddress
{
   public static void main(String args[]) throws Exception
   {
       String s = InetAddress.getLocalHost().toString();
       StringTokenizer st = new StringTokenizer(s,"/");
       st.nextToken();
       String ip = st.nextToken();
       System.out.println(ip);
   }
}
