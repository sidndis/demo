
import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class FanAnimation extends Applet implements ActionListener,Runnable
{
    Button start,stop;
    int st_ang = 10;
    int st_ang1 = 110;
    int st_ang2 = 190;
    int st_ang3 = 280;
    Thread t;
    Thread t1;
    Thread t2;
    Thread t3;
    public void init()
    {
        start = new Button("Start");
        stop = new Button("Stop");
        
        add(start);
        add(stop);
        
        start.addActionListener(this);
        stop.addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==start)
        {
            t = new Thread(this);
            t.start();
            
            t1 = new Thread(this);
            t1.start();
            
            t2 = new Thread(this);
            t2.start();
            
            t3 = new Thread(this);
            t3.start();
        }
        else if(e.getSource()==stop)
        {
            t.stop();
            t1.stop();
            t2.stop();
            t3.stop();
        }
    }
    
    public void paint(Graphics g)
    {
        g.setColor(Color.red);
        g.drawOval(50 , 50 , 200 , 200);
        
        g.setColor(Color.black);
        g.fillArc(60 , 60 , 180 , 180 , st_ang , 30);
        g.fillArc(60 , 60 , 180 , 180 , st_ang1 , 30);
        g.fillArc(60 , 60 , 180 , 180 , st_ang2 , 30);
        g.fillArc(60 , 60 , 180 , 180 , st_ang3 , 30);
    }
    public void run() 
    {
        if(Thread.currentThread()==t)
        {
            while(true)
            {
                st_ang = st_ang + 10;
                st_ang1 = st_ang1 + 10;
                st_ang2 = st_ang2 + 10;
                st_ang3 = st_ang3 + 10;
                try 
                {
                    Thread.sleep(50);

                    if(st_ang==360)
                    {
                        st_ang=0;
                    }
                    /*if(st_ang1==360)
                    {
                        st_ang1=0;
                    }
                    if(st_ang2==360)
                    {
                        st_ang2=0;
                    }
                    if(st_ang3==360)
                    {
                        st_ang3=0;
                    }*/
                }
                catch(Exception e)
                {

                }
                repaint();
            }
        }
        
        else if(Thread.currentThread()==t1)
        {
            while(true)
            {
                st_ang1 = st_ang1 + 10;
                try 
                {
                    Thread.sleep(50);

                    if(st_ang1==360)
                    {
                        st_ang1=0;
                    }
                }
                catch(Exception e)
                {

                }
                repaint();
            }
        }
        
        else if(Thread.currentThread()==t2)
        {
            while(true)
            {
                st_ang2 = st_ang2 + 10;
                try 
                {
                    Thread.sleep(50);
                    if(st_ang2==360)
                    {
                        st_ang2=0;
                    }
                }
                catch(Exception e)
                {

                }
                repaint();
            }
        }
        
        else if(Thread.currentThread()==t3)
        {
            while(true)
            {
                st_ang3 = st_ang3 + 10;
                try 
                {
                    Thread.sleep(50);
                    
                    if(st_ang3==360)
                    {
                        st_ang3=0;
                    }
                }
                catch(Exception e)
                {

                }
                repaint();
            }
        }
    }
}
