import java.awt.event.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;
import javax.swing.*;


public class ChattingPub implements ActionListener
{
    ServerSocket sc;
    Socket s;
    PrintWriter pw;
    BufferedReader br;
    BufferedReader key;
    Thread t1,t2;
    JFrame f;
    JTextArea ta;
    JTextField tf;
    JButton bt;
    
    public ChattingPub() 
    {
        try
        {
            sc = new ServerSocket(9000);
            //System.out.println("Server Started");
            s = sc.accept();
            //System.out.println("connection built");
            pw = new PrintWriter(s.getOutputStream());
            br = new BufferedReader(new InputStreamReader(s.getInputStream()));
            //key = new BufferedReader(new InputStreamReader(System.in));
            //t1 = new Thread(new Sender());
            t2 = new Thread(new Receiver());
            //t1.start();
            t2.start();
            f = new JFrame(); 
            
            f.setLayout(null);
            f.setVisible(true);
            
            
            
            ta = new JTextArea();
            ta.setBounds(20, 20, 460, 410);
            ta.setEditable(false);
            f.add(ta);

            tf = new JTextField();
            tf.setBounds(20, 450, 340, 30);
            f.add(tf);

            bt = new JButton("Send");
            bt.setBounds(380, 450, 100, 30);
            bt.addActionListener(this);
            f.add(bt);

            f.setSize(520, 520);
            //f.setDefaultCloseOperation();
            tf.addKeyListener(new KeyAdapter() {
        public void keyPressed(KeyEvent e)
        {
            if(e.getKeyCode()==10)
            {
                doSend();
            }
            
        }
        });
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    
    
    public static void main(String[] args) 
    {
       
        ChattingPub chat1 = new ChattingPub();
    }
    
    public void actionPerformed(ActionEvent e)
    {
        doSend();
    }
    
    void doSend()
    {
        String msg = tf.getText();
        pw.println(msg);
        pw.flush();
        tf.setText("");
        String m = ta.getText();
        ta.setText(m+"me : "+msg+"\n");
    }

    

    
//    class Sender implements Runnable
//    {
//
//        public Sender() 
//        {
//        
//        }
//    
//        public void run()
//        {
//            try
//            {
//                while(true)
//                {
//                    String msg = key.readLine();     
//                    pw.println(msg);
//                    pw.flush();
//                }
//            }
//            catch (Exception e) 
//            {
//                e.printStackTrace();
//            }
//        }
//    }
    
    class Receiver implements Runnable
    {

        public Receiver() 
        {
        
        }
    
        public void run()
        {
            try
            {
                while(true)
                {
                    String msg = br.readLine();
                    String s = ta.getText();
                    ta.setText(s+"friend : "+msg+"\n");
                }
            }
            catch (Exception e) 
            {
                e.printStackTrace();
            }
        }
    }
    
    
}


