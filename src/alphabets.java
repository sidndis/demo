
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class alphabets 
{
    public static void main(String[] args) throws Exception
    {
        String s[]; 
        int n;
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.print("Enter number of roll numbers : ");
        n = Integer.parseInt(br.readLine());
        s = new String[n];
        for(int i = 0;i<n;i++)
        {
            System.out.print("Enter Roll number "+(i+1)+" : ");
            s[i] = br.readLine();
            
        }
        try
        {
            for(int i = 0;i<n;i++)
            {
                StringTokenizer st = new StringTokenizer(s[i],"-");
                String l,d;
                l = st.nextToken();
                d = st.nextToken();
                int countL = l.length();
                int countD = d.length();
                if(!(countL == 3 && countD == 4))
                {
                    System.out.println("Invalid");
                }
                else
                {
                        l = l.toUpperCase();
                        int val[] = new int[3];
                        boolean flag = true;
                        for(int j =0;j<3;j++)
                        {
                            if(l.charAt(j)>=65 && l.charAt(j)<=90)
                            {
                                val[j] = l.charAt(j)-65;
                            }
                            else
                            {
                                    flag = false;
                            }
                        }
                        if(flag)
                        {
                            double sum = (val[0]*Math.pow(26, 2)+val[1]*Math.pow(26, 1)+val[2]*Math.pow(26, 0));
                            int no = Integer.parseInt(d);
                            if(Math.abs(sum-no) <= 100)
                                System.out.println("nice");
                            else
                                System.out.println("not nice");
                        }
                        else
                            System.out.println("Invalid");


                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Invalid");
        }
    }
}
