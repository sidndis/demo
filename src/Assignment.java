import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class Assignment extends JFrame implements ActionListener
{

    JTable jt;
    String data[][];
    JTextField tf1,tf2,tf3;
    JButton bt1,bt2;
    int count;
    int row;
    
    public Assignment()
    {
        data = new String[5][3];
        fileReading();
        this.setVisible(true);
        this.setSize(500, 500);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setLayout(null);
        
        InnerTable tm = new InnerTable();
        
        jt=new JTable();
        jt.setModel(tm);
        jt.setBounds(20, 20, 200, 200);
        
        tf1=new JTextField();
        tf1.setBounds(300, 300, 100, 30);
        this.add(tf1);
        
        tf2=new JTextField();
        tf2.setBounds(300, 350, 100, 30);
        this.add(tf2);
        
        tf3=new JTextField();
        tf3.setBounds(300, 400, 100, 30);
        this.add(tf3);
        
        bt1= new JButton("<<");
        bt1.setBounds(320, 450, 25, 25);
        this.add(bt1);
        bt1.addActionListener(this);
        
        bt2= new JButton(">>");
        bt2.setBounds(355, 450, 25, 25);
        this.add(bt2);
        bt2.addActionListener(this);
        
        this.add(jt);
        
        
        jt.addMouseListener(new MouseAdapter() 
        {
            public void mouseClicked(MouseEvent e)
            {
                row=jt.getSelectedRow();
                tf1.setText(data[row][0]);
                tf2.setText(data[row][1]);
                tf3.setText(data[row][2]);
            }
        });
      
        
        
    }
    
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==bt1)
        {
            row--;
        }
        else if(e.getSource()==bt2)
        {
            row++;
        }
         tf1.setText(data[row][0]);
                tf2.setText(data[row][1]);
                tf3.setText(data[row][2]);
    }
    
    void fileReading()
    {
        try
        {
            FileReader fr = new FileReader("F:\\hello.txt");
            BufferedReader br = new BufferedReader(fr);
            
            String s;
            
            while(true)
            {
                s = br.readLine();
                if(s==null)
                    break;
                StringTokenizer st = new StringTokenizer(s, "#");
                for(int i=0;i<3;i++)
                {
                        data[count][i]=st.nextToken();
                    //System.out.println(st.nextToken());
                }
                count++;
            }
            
            
            
        }
        catch (Exception ex) 
        {
            ex.printStackTrace();
        }
    }
    
    
    class InnerTable extends AbstractTableModel
    {

        @Override
        public int getRowCount() 
        {
            return data.length;
        }

        @Override
        public int getColumnCount() 
        {
            return 3;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) 
        {
            return data[rowIndex][columnIndex];
        }
        
    }
    
    
    public static void main(String[] args)
    {
        Assignment a1=new Assignment();
    }
}
