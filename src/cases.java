
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class cases
{
    public static void main(String[] args) throws IOException 
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        
        BufferedReader br = new BufferedReader(isr);
        
        String n;
        int result[] = new int[12];
        System.out.println("Enter the Number : ");
        
        n = br.readLine();
        if(n.length()!=5)
        {
            System.out.println("Invalid");
        }
        else
        {
            
            int a,b,c,d,e;
            a=b=c=d=e=0;
            try 
            {
                a = Integer.parseInt(""+n.charAt(0));
                b = Integer.parseInt(""+n.charAt(1));
                c = Integer.parseInt(""+n.charAt(2));
                d = Integer.parseInt(""+n.charAt(3));
                e = Integer.parseInt(""+n.charAt(4));
            }
            catch (Exception ex)
            {
                System.out.println("Invalid input");
            }
            int n1,n2,n3;
            n1=n2=n3=0;
            for(int i =0;i<12;i++)
            {
                switch(i)
                {
                    
                    case 0:
                    case 1:
                    n1=a;
                    n2=b;
                    n3=c*100+d*10+e;
                    break;
                    case 2:
                    case 3:
                    n1=a*10+b;
                    n2=c;
                    n3=d*10+e;
                    break;
                    case 4:
                    case 5:
                    n1=a*100+b*10+c;
                    n2=d;
                    n3=e;
                    break;
                    case 6:
                    case 7:
                    n1=a*10+b;
                    n2=c*10+d;
                    n3=e;
                    break;
                    case 8:
                    case 9:
                    n1=a;
                    n2=b*100+c*10+d;
                    n3=e;
                    break;
                    case 10:
                    case 11:
                    n1=a;
                    n2=b*10+c;
                    n3=d*10+e;
                    break;
                    
                }
                if(i%2==0)
                    result[i] = Math.abs(n1+n2-n3);
                else
                    result[i] = Math.abs(n1-n2+n3);
            }
            
            
        }
        Arrays.sort(result);
        System.out.println("Minimum value : "+result[0]);
        System.out.println("Maximum value : "+result[result.length-1]);
        
    }
}

