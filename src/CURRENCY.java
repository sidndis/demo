
import java.io.BufferedReader;
import java.io.InputStreamReader;


public class CURRENCY 
{
    
    int[] curr_arr = new int[] { 5, 10, 20, 50, 100, 200 , 500 };       
    
    int ways(int curr_arr[], int index, int value) 
    {
        if (value == 0)
        {
            return 1;
        }
        if (value < 0 || index <= 0 )
        {
            return 0;
        }
        int withthout = ways(curr_arr, index - 1, value); 
        int with = ways(curr_arr, index, value - curr_arr[index - 1]); 
        return withthout + with;
    }

    public CURRENCY() throws Exception
    {
        int n;
        double curr[];
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        System.out.print("Enter number of currencies : ");
        n = Integer.parseInt(br.readLine());
        curr = new double[n];
        for(int i = 0;i<n;i++)
        {
            System.out.print("Enter currency "+(i+1)+" : ");
            curr[i] = Double.parseDouble(br.readLine());
        }
        
        for(int i = 0;i<n;i++)
        {
            int b = (int)(curr[i]*100);
            if(!(b>=5 && b<=3000))
            {
                System.out.println("Invalid Currency Amount");
            }
            else
            {
                System.out.println(ways(curr_arr, curr_arr.length,b));
            }
        }
    }
    
    public static void main(String[] args) throws Exception
    {
        new CURRENCY();
    }
    
}

