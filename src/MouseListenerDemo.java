
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;


public class MouseListenerDemo extends JFrame
{
    JLabel lb;
    public MouseListenerDemo() 
    {
        this.setLayout(null);
        lb = new JLabel();
        lb.setBounds(50, 50, 100, 30);
        Dimension screensize=Toolkit.getDefaultToolkit().getScreenSize();
        
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        setBounds(0,0,screensize.width,screensize.height);
        this.add(lb);
        setVisible(true);
        this.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e)
            {
                int x = e.getXOnScreen();
                int y = e.getYOnScreen();
                int z = e.getClickCount();
                int a = e.getButton();
                lb.setText(""+x+","+y+"."+z+";"+a);
               
                
            }

            @Override
            public void mousePressed(MouseEvent e) {
                
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                
            }

            @Override
            public void mouseExited(MouseEvent e) {
                
            }
        });
    }
    public static void main(String[] args) 
    {
        MouseListenerDemo m = new MouseListenerDemo();
    }
 
}
