
import java.applet.*;
import java.awt.event.*;
import java.awt.*;
import java.util.ArrayList;

public class Applet3 extends Applet implements MouseMotionListener
{
    Position p;
    ArrayList<Position> al;
    public void init()
    {
        
        al=new <Position>ArrayList();
        this.addMouseMotionListener(this);
        //p=new Position();
        this.addMouseListener(new MouseAdapter()
        {
            public void mouseReleased(MouseEvent e)
            {
                p=new Position();
                p.x=0;
                p.y=0;
                al.add(p);
                //System.out.println("Mouse released");
            }   
        });
    }
    
    public void paint(Graphics g)
    {
        for (int i=0;i<al.size()-1;i++) 
        {
            if((al.get(i).x==0 && al.get(i).y==0) || (al.get(i+1).x==0 && al.get(i+1).y==0))
                continue;
            else
            g.drawLine(al.get(i).x ,al.get(i).y ,al.get(i+1).x ,al.get(i+1).y);
        }
    }
    
    public void mouseMoved(MouseEvent e)
    {
    }
    public void mouseDragged(MouseEvent e)
    {
//        System.out.println("x = "+e.getX());
//        System.out.println("y = "+e.getY());
        p=new Position();
        p.x = e.getX();
        p.y = e.getY();
        al.add(p);
        repaint();
    }
}

class Position
{
    int x,y; 
}

