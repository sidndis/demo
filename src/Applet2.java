
import java.applet.*;
import java.awt.event.*;
import java.awt.*;

public class Applet2 extends Applet implements ActionListener
{
    TextField tf1,tf2,tf3,tf4;
    Button bt;
    int s1=30,s2=60,s3=90,s4=180;
    public void init()
    {
        setLayout(null);
        
        tf1 = new TextField();
        tf2 = new TextField();
        tf3 = new TextField();
        tf4 = new TextField();
        
        tf1.setBounds(20,20,50,20);
        tf2.setBounds(80,20,50,20);
        tf3.setBounds(140,20,50,20);
        tf4.setBounds(200,20,50,20);
        
        this.add(tf1);
        this.add(tf2);
        this.add(tf3);
        this.add(tf4);
        
        this.setSize(1000, 600);
        
        bt=new Button("Draw");
        bt.setBounds(260,20,50,20);
        this.add(bt);
        bt.addActionListener(this);
    }
    
    public void paint(Graphics g)
    {
       g.setColor(Color.red);
       g.fillArc(100, 100, 200, 200, 0, s1);
       g.setColor(Color.GREEN);
       g.fillArc(100, 100, 200, 200, s1, s2);
       g.setColor(Color.blue);
       g.fillArc(100, 100, 200, 200, s1+s2, s3);
      
       g.setColor(Color.YELLOW);
       g.fillArc(100, 100, 200, 200, s1+s2+s3,s4 );
   }
    
    public void actionPerformed(ActionEvent e)
    {
        s1=Integer.parseInt(tf1.getText());
        s2=Integer.parseInt(tf2.getText());
        s3=Integer.parseInt(tf3.getText());
        s4=Integer.parseInt(tf4.getText());
        repaint();
    }
}
