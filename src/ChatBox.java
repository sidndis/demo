import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class ChatBox extends JFrame implements ActionListener
{
    JTextArea ta;
    JTextField tf;
    JButton bt;
    public ChatBox()
    {
        this.setLayout(null);
        this.setVisible(true);
        
        ta = new JTextArea();
        ta.setBounds(20, 20, 460, 410);
        ta.setEditable(false);
        this.add(ta);
        
        tf = new JTextField();
        tf.setBounds(20, 450, 340, 30);
        this.add(tf);
        
        bt = new JButton("Send");
        bt.setBounds(380, 450, 100, 30);
        bt.addActionListener(this);
        this.add(bt);
        
        this.setSize(520, 520);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    
   
    
//    public static void main(String[] args) 
//    {
//        ChatBox cb = new ChatBox();
//    }

    
    public void actionPerformed(ActionEvent e) 
    {
        String s = tf.getText();
        tf.setText("");
        String s1 = ta.getText();
        ta.setText(s1+s+"\n");
    }
    
}
