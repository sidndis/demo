
import java.io.*;
import java.util.StringTokenizer;
import sun.util.locale.StringTokenIterator;

public class Number 
{
    public static void main(String[] args) throws Exception
    {
        InputStreamReader isr = new InputStreamReader(System.in);
        
        BufferedReader br = new BufferedReader(isr);
        
        int t,count,flag,flag2;
        
        System.out.println("Enter the Number of cases : ");
        
        t = Integer.parseInt(br.readLine());
        
        int r[] = new int[t];
        int n[] = new int [t];
        String str_arr[] = new String [t];
        
        int arr[][] = new int[t][];
        
        for(int i=0;i<t;i++)
        {
            String input[] = new String[t];
            
            System.out.println("Enter the number of elements and value of r :");
            input[i] = br.readLine();
            
            StringTokenizer stoken = new StringTokenizer(input[i]);
            
            n[i] = Integer.parseInt(stoken.nextToken());
            r[i] = Integer.parseInt(stoken.nextToken());
            
            str_arr[i] = br.readLine();
            
            StringTokenizer stoken_arr = new StringTokenizer(str_arr[i]);
            
            arr[i] = new int[n[i]];
            
            for(int j=0;j<n[i];j++)
            {
                arr[i][j] = Integer.parseInt(stoken_arr.nextToken());
            }
        }
        for(int i=0;i<t;i++)
        {
            flag2=1;
            for(int j=0;j<n[i];j++)
            {
                count = 0;
                flag=1;
                
                for(int k=j-1;k>=0;k--)
                {

                    if(arr[i][j]==arr[i][k])
                    {
                        flag=0;
                        break;
                    }
                }
                if(flag==1)
                {
                    for(int l=j;l<n[i];l++)
                    {
                        if(arr[i][j]==arr[i][l])
                        {
                            count++;
                        }
                    }
                    if(count>=r[i])
                    {
                        System.out.print(arr[i][j]+" ");
                        flag2=0;
                    }
                }
            }
            if(flag2==1)
            {   
                System.out.println("");
                System.out.print("NOT FOUND");
            }
        }
    }
}
