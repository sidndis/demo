
import java.applet.*;
import java.awt.event.*;
import java.awt.*;

public class Applet1 extends Applet implements ActionListener
{
    TextField tf1,tf2,tf3,tf4;
    Button bt;
    int s1=50,s2=100,s3=240,s4=300;
    public void init()
    {
        setLayout(null);
        
        tf1 = new TextField();
        tf2 = new TextField();
        tf3 = new TextField();
        tf4 = new TextField();
        
        tf1.setBounds(20,20,50,20);
        tf2.setBounds(80,20,50,20);
        tf3.setBounds(140,20,50,20);
        tf4.setBounds(200,20,50,20);
        
        this.add(tf1);
        this.add(tf2);
        this.add(tf3);
        this.add(tf4);
        
        this.setSize(1000, 600);
        
        bt=new Button("Draw");
        bt.setBounds(260,20,50,20);
        this.add(bt);
        bt.addActionListener(this);
    }
    
    public void paint(Graphics g)
    {
        g.drawLine(120, 500, 820 , 500);
        g.drawLine(120, 500, 120 , 100);
        g.drawRect(140,500-s1, 30, s1);
        g.drawRect(190,500-s2, 30, s2);
        g.drawRect(240,500-s3, 30, s3);
        g.drawRect(290,500-s4, 30, s4);
    }
    
    public void actionPerformed(ActionEvent e)
    {
        s1=Integer.parseInt(tf1.getText());
        s2=Integer.parseInt(tf2.getText());
        s3=Integer.parseInt(tf3.getText());
        s4=Integer.parseInt(tf4.getText());
        repaint();
    }
}
